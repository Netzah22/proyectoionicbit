import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formulario: FormGroup;

  constructor(private formB: FormBuilder, private router: Router, private userS: UsersService) { }

  ngOnInit() {
    this.createForm();
  }

  // get usuarioInvalido(){
  //   return this.formulario.get("user").invalid && this.formulario.get("user").touched
  // }

  createForm(){
    this.formulario = new FormGroup({
      usuario: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  logueo(){
    console.log(this.formulario);
    this.userS.login(this.formulario.value).subscribe(
      (data:any)=>{
        if(!data.token){
          console.log(data);
          alert('Usuario o contraseña incorrectos');
        }else{
          console.log("Logueo correcto");
          this.router.navigateByUrl("/home");
        }
      }
    )
  }

}
