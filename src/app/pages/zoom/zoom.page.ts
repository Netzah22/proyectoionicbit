import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import ZoomMtgEmbedded from '@zoomus/websdk/embedded';

@Component({
  selector: 'app-zoom',
  templateUrl: './zoom.page.html',
  styleUrls: ['./zoom.page.scss'],
})
export class ZoomPage implements OnInit {

  signatureEndpoint = 'http://localhost:4000'
  apiKey = '9dUehahjQWCT9dWQGpwx7Q'
  meetingNumber = '8343783739'
  role = 0
  userName = 'Angular'
  userEmail = ''
  passWord = 'HolaPuerco'
  registrantToken = ''

  client = ZoomMtgEmbedded.createClient();
  
  constructor(public httpClient: HttpClient) { }

  ngOnInit() {

    let meetingSDKElement = document.getElementById('meetingSDKElement');

    this.client.init({
      debug: true,
      zoomAppRoot: meetingSDKElement,
      language: 'en-US',
      customize: {
        meetingInfo: ['topic', 'host', 'mn', 'pwd', 'telPwd', 'invite', 'participant', 'dc', 'enctype'],
        toolbar: {
          buttons: [
            {
              text: 'Custom Button',
              className: 'CustomButton',
              onClick: () => {
                console.log('custom button');
              }
            }
          ]
        }
      }
    });
  }

  getSignature() {

    // document.getElementById('zmmtg-root').style.display = 'block'

    this.httpClient.post(this.signatureEndpoint, {
	    meetingNumber: this.meetingNumber,
	    role: this.role
    }).toPromise().then((data: any) => {
      if(data.signature) {
        console.log(data.signature)
        this.startMeeting(data.signature)
      } else {
        console.log(data)
      }
    }).catch((error) => {
      console.log(error)
    })
  }

  startMeeting(signature) {

    this.client.join({
    	apiKey: this.apiKey,
    	signature: signature,
    	meetingNumber: this.meetingNumber,
    	password: this.passWord,
    	userName: this.userName,
      userEmail: this.userEmail,
      tk: this.registrantToken
    })
  }

}
