import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PopoverComponent } from './popover/popover.component';
import { ZoomComponent } from './zoom/zoom-new.component';



@NgModule({
  declarations: [
    PopoverComponent,
    ZoomComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
