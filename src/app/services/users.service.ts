import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../models/login.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  URL='http://localhost:3000/';

  login(login: Login):Observable<Request>{
    return this.http.post<Request>(`${this.URL}innov/login`, login);
  }
}
